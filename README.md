# Assignment A03Onteddu
Adding Node, Express, BootStrap, EJS to our personal page.
## How to use
Clone Repo
Run npm install to install all the dependencies in the package.json file.
Run node server.js to start the server. (Hit CTRL-C to stop.)
```
> npm install
> node sever.js
```
Point your browser to `http://localhost:8081. 